HELP_DIR="${SRC_LIB_DIR}/../help"

help() {
    NAME=${1:-kit}
    cat "${HELP_DIR}/${NAME}.txt"
    if [ "${NAME}" == kit ] ; then
        cat "${HELP_DIR}/commands.txt"
    fi
}
