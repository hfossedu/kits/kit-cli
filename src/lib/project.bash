import curl
import glab
import gh
import rawurlencode

project_exists() {
    gitlab_project_exists() {
        local url="${1}"
        local path
        local endpoint
        path="$(path_from_url "${url}")"
        path="$(rawurlencode "${path}")"
        endpoint="projects/${path}"
        gitlab_api "${endpoint}" --request GET
    }

    github_project_exists() {
        local url
        local path
        url="${1}"
        path="$(path_from_url "${url}")"
        gh api "repos/${path}" &>/dev/null
    }

    if is_github_url "$1" ; then
        github_project_exists "$@"
    else
        gitlab_project_exists "$@"
    fi
}

project_create() {
    gitlab_project_create() {
        local url="${1}"
        local path
        local group
        local project
        local gid
        path="$(path_from_url "${url}")"
        group="$(dirname "${path}")"
        project="$(basename "${path}")"
        gid="$(namespace_get_id "${group}")"
        gitlab_api "projects" --request POST \
            --header "Content-Type: application/json" \
            --data '{
                "name": "'"${project}"'",
                "namespace_id": "'"${gid}"'"
            }'
    }

    github_project_create() {
        local url="${1}"
        local path
        local group
        local project
        path="$(path_from_url "${url}")"
        gh repo create "${path}" --private &>/dev/null
    }

    if is_github_url "$1" ; then
        github_project_create "$@"
    else
        gitlab_project_create "$@"
    fi
}

gitlab_api() {
    local endpoint="$1" ; shift
    api_url="https://gitlab.com/api/v4/${endpoint}"
    curl --url "${api_url}" --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "$@"
}

project_delete() {
    gitlab_project_delete() {
        local url="$1"
        local path
        path="$(path_from_url "${url}")"
        glab repo delete "$path" --yes
    }

    github_project_delete() {
        local url="$1"
        local path
        path="$(path_from_url "${url}")"
        gh repo delete "$path" --yes
    }

    if is_github_url "$1" ; then
        github_project_delete "$@"
    else
        gitlab_project_delete "$@"
    fi

}

path_from_url() {
    local url="$1"
    local result
    remove_dotgit_suffix() {
        echo "${1%.git}"
    }

    remove_https_server_prefix() {
        local t
        t="${1#*//}"
        t="${t#*/}"
        echo "$t"
    }

    result="${url}"
    result="$(remove_https_server_prefix "${result}")"
    result="$(remove_dotgit_suffix "${result}")"
    echo "${result}"
}

namespace_get_id() {
    group="$1"
    query='query { namespace(fullPath: "'${group}'") { id } }'
    result="$(glab api graphql -f "query=${query}")"
    # => { data : { namespace: {id: gid://gitlab/Namespaces::UserNamespace/646224, other junk} } }
    gid="$(echo "$result" | jq -r '.data.namespace.id')"
    # => gid://gitlab/Namespaces::UserNamespace/646224
    basename "$gid"
    # => 646224
}

error_if_github_url() {
    local url
    url="${1}"
    if is_github_url "${url}" ; then
        echo "GitHub not supported (url=${url})"
        >&2 echo "GitHub not supported"
        exit 1
    fi
}

is_github_url() {
    local url
    url="${1}"
    echo "${url}" | grep -E "github\.com" &>/dev/null
}
