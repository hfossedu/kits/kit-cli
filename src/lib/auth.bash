#shellcheck disable=SC2016
authorize_git_with_gitlab() {
    git config --global \
        --replace-all \
            credential.https://gitlab.com.helper \
            '!f() { printf "%s\n" "username=oath2" "password=$GITLAB_TOKEN"; };f'
}
