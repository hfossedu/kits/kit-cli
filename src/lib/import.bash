IMPORT_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]:-$0}";)" &>/dev/null && pwd 2>/dev/null;)";
export IMPORT_DIR

import() {
    source "${IMPORT_DIR}/${1}.bash"
}
