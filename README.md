# Kit CLI

A tool to capture and deploy kit artifacts.

## Status: experimental

This tool is actively under development and its interface can change at anytime.

## Quick start Linux or MacOS

1. Install and run Docker.
1. Clone this repo.
1. Run bin/build.sh
1. Add this function to your .bashrc or .zshrc

    ```bash
    kit() {
        docker run -it --rm \
            -v "${PWD}:/kit" -w "/kit" \
            kit-cli "$@"
    }
    ```

1. Start a new terminal and use `kit`:

    ```bash
    kit help
    ```

## Community

* Obey and uphold our [Code of Conduct](docs/IMPORTANT/CODE_OF_CONDUCT.md)
* Join the [Kits Discord server](https://discord.gg/x2Hn4BFFnW)

## License

* Code licensed under [GPLv3 or higher](docs/IMPORTANT/LICENSE_FOR_CODE.txt)
* Content licensed under [CC-BY-SA 4.0 or higher](docs/IMPORTANT/LICENSE_FOR_NON_CODE.txt)

## Developers

See [developer documentation](docs/Developer.md).
