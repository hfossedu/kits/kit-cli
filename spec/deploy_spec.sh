Describe "kit deploy:"

    BeforeEach 'tempdir_enter ; reset_mocked_traces'
    AfterEach tempdir_exit

    namespace_get_id() {
        # Queries GitLab for the ID number of a given group.
        # Uses glab graphql + jq to get this done.
        # We mock this out an return a pre-baked ID number.
        echo "646224"
    }

    Context "kit deploy <empty-project-pathname> <non-existant-url>"
        It "uses curl to create an empty project on GitLab."
            When call _kit deploy test https://gitlab.com/SomeGroup/deploy-test

            assert_curl_called_with_args() {
                set +u
                while [ -n "${1}" ] ; do
                    Assert [[ "${MOCKED_CURL_TRACE[0]}" =~ "${1}" ]]
                    shift
                done
                set -u
            }

            assert_curl_called_with_args \
                "--header PRIVATE-TOKEN: bogus-gitlab-token" \
                "--request POST" \
                '"name": "deploy-test"' \
                '"namespace_id": "646224"'
        End

        It "uses gh to create an empty project on GitHub."
            When call _kit deploy test https://github.com/SomeOrg/deploy-test

            assert_gh_called_with_args() {
                set +u
                while [ -n "${1}" ] ; do
                    Assert [[ "${MOCKED_GH_TRACE[0]}" =~ "${1}" ]]
                    shift
                done
                set -u
            }

            assert_gh_called_with_args \
                '^gh repo create SomeOrg/deploy-test --private$'
        End
    End

End
