Describe "help"
    Context "kit"
        It "displays the top-level usage message."
            When call _kit
            The output should include "kit <command>"
        End
    End

    Context "kit help"
        It "displays the top-level usage messgae."
            When call _kit help
            The output should include "kit <command>"
        End
    End

    Context "kit help help"
        It "displays help's help"
            When call _kit help help
            The output should include "kit help"
        End
    End

    Context "kit help capture"
        It "displays capture help"
            When call _kit help capture
            The output should include "kit capture"
        End
    End

    Context "kit help deploy"
        It "displays deploy help"
            When call _kit help deploy
            The output should include "kit deploy"
        End
    End
End
