source spec/lib/paths.bash

export TEMP
TEMP="${PWD}/.test-temp"

tempdir_enter() {
  make_paths "${TEMP}"
  pushd_ "${TEMP}"
}

tempdir_exit() {
  popd_
  rm -rf "${TEMP}";
}
