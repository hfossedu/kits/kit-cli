make_paths() {
    set +u
    while [ -n "${1}" ]
    do
        mkdir -p "${1}" &> /dev/null
        shift
    done
    set -u
}

pushd_() {
    \command pushd "${1}" &> /dev/null || exit 1
}

popd_() {
    \command popd &> /dev/null || exit 1
}
