# Sketches

These are non-authoritative documentation. They are the results of ideation
and design. They are not law. They may change at any time. They are here
to help developers.

## Command syntax

### Capture

```bash
kit capture [<artifact> ...] <url> [to <pathname>]

# Capture repo of project at <url> to a locale based on url's path.
kit capture repo <url>

# Capture repo of project at <url> to locally named project x/y/z.
kit capture issues <url> to x/y/z
```

* Adv: Small syntax
* Dis: Order matters.
* New options will require flag notation (e.g., --flag).
* Not clear if future commands can fit this same syntax.

### Deploy

```bash
kit deploy [<artifact> ...] <pathname> to <url>
```

### Edit

Because repositories are stored as archives of bare repositories, to edit them,
they must be extracted and cloned. That's what `kit open` does.

```bash
# Opens a repo for editing.
kit open <artifact> <pathname> [as <path>]
```

To make changes to an open repo, modify the clone as you would a local repo,
and push your changes to `origin` which is the bare repo that you are editing.
Only pushed changes will be reflected in the captured repo.

When you are done editing, you must close the repo so that the bare repo
is properly archived and replaces the old repo. That's what `kit close` does.
It also deletes the "clone".

```bash
# Close a repo when done editing.
kit close <artifact> <pathname>
```

Finally, don't forget to stage and commit the updated, archived, bare repo
to the kit's repository. `kit close` does not do that.

## A Kit's file structure

```bash
# Its root contains normal project files. These are just an example.
/
    README.md
    LICENSE.md
    .gitlab-ci.yaml
    .gitignore
    .gitattributes
    bin/
    docs/
        developer/
        user/
    tests/

    # Instructional materials are placed in materials/
    materials/

    # .kit/ is where kit stores stuff.
    .kit/
        # artifacts/ is where captured artifacts are stored.
        artifacts/
            # Folders represent groups and subgroups...
            g/
                h/
                    # Groups an subgroups can have artifacts too.
                    # For example, here are some labels from gitlab.
                    labels.gitlab.json
                    # Unless they contain a project file. Then its a project.
                    p/
                        # Marks parent as a project
                        project
                        # Archived bare repo
                        repo.git.tar.bz2
                        # Issues in a format for GitHub.
                        issues.github.json
                        # Issues in a format for GitLab.
                        issues.gitlab.json
                        # Labels in a format for GitHub.
                        labels.github.json
                        # Labels in a format for GitLab.
                        labels.gitlab.json
                        # Each feature is implemented by augmenting captured
                        # artifacts.
                        features/
                        # Settings in GitLab format.
                        settings.gitlab.json
                        # Settings in GitHub format.
```

The above suggests duplicating data for each service (GitHub GitLab).
In the future we could try to create a unified format to save space.
For now, we plan to have conversion scripts, one for each file and direction.
For example: convert-gitlab-to-github-issues, convert-github-to-gitlab-issues,
etc. If we ever add another service, to avoid a combinatorial explosion of
conversion scripts, we should consider identifying a canonical format, and
then write converters to and from that format from each of the service specific
formats. For now, YAGNI to both optimizations.
