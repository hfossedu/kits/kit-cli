# License for non-code

Non-code works in this project is licensed under a Creative Commons
Attribution-ShareAlike 4.0 International License. http://creativecommons.org/licenses/by-sa/4.0/

To attribute this work to its authors, please provide a link back to this project.
