# Developer Docs

## Ground Rules

1. All community members agree to uphold and obey our [code of conduct][coc].
1. Copyrights and licenses
   1. All contributors retain the copyright to their contribution.
   1. All contributors agree to license their code contribution under
      [GPL 3.0][gpl] or higher
   1. All contributors agree to license their non-code contributions under
      [CC-BY-SA 4.0][cc] or higher.
   1. All contributors sign of on the
      [Developer Certificate of Origin (DCO)][dco] when they offer a
      contribution.

[coc]: IMPORTANT/CODE_OF_CONDUCT.md
[gpl]: IMPORTANT/LICENSE_FOR_CODE.txt
[cc]: IMPORTANT/LICENSE_FOR_NON_CODE.txt
[dco]: IMPORTANT/DCO.txt

## Setup Development Environment

1. Install and run Docker.
1. Clone this project.
1. Configure testing environment by editing `.env`.
1. We recommend using VS Code for development.

## Quick tour

Filesystem captures on 5/11/2023

```bash
.
├── Dockerfile
├── bin              # Developer commands; read them, use them.
│   ├── build.bash
│   ├── lint.bash
│   ├── run.bash
│   └── test.bash
├── docs
│   ├── Developer.md
│   ├── IMPORTANT
│   │   ├── CODE_OF_CONDUCT.md
│   │   ├── DCO.txt
│   │   ├── LICENSE_FOR_CODE.txt
│   │   └── LICENSE_FOR_NON_CODE.txt
│   └── User.md
├── spec             # SellSpec scripts for BDD development. Test first please!
│   ├── capture_spec.sh
│   ├── deploy_spec.sh
│   └── spec_helper.sh
└── src
    ├── bin          # User commands. Keep it small.
    │   └── kit
    └── lib          # All the functionality in source-able scripts.
        ├── _kit.bash      # Top-level library that loads all the others.
        ├── capture.bash
        ├── curl.bash
        ├── deploy.bash
        ├── gh.bash
        ├── glab.bash
        ├── import.bash
        ├── project.bash
        └── rawurlencode.bash

8 directories, 24 files
```

## Unit Tests

* Run `bin/test.bash`
* Implemented using the shellspec framework.
* Do not make remote calls.
* Fast.
* Write them first, run them often.

## Linters

* Run `bin/lint.bash`
* Run often.

## Manual Tests

* Create or identify a group/organization on GitHub and GitLab for testing.
* Define GH_TOKEN and GITLAB_TOKEN to PATs with enough permissions over
  these groups/organizations for the operations you are testing.
* Run `bin/shell-live.bash` ; starts bash inside a container of the most local
  image.
* Use `kit`.
