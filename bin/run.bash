#!/usr/bin/env bash
BIN_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]:-$0}";)" &>/dev/null && pwd 2>/dev/null;)";

cd "$BIN_DIR/.." || exit
bin/build.bash
docker run -it --rm \
    --env-file .env \
    -v "${PWD}:/kit" \
    kit "$@"
