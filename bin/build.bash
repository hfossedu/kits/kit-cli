#!/usr/bin/env bash
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}/.." || exit 1

# Name to use for local builds.
LOCAL_IMAGE_NAME="kit"

if [[ -n "$PIPELINE_BUILDX_BUILD_OPTIONS" ]] ; then
    # PIPELINE_BUILDX_BUILD_OPTIONS should not be quoted as it may have multiple
    # space-sperated options.
    # shellcheck disable=SC2086
    docker buildx build "$@" $PIPELINE_BUILDX_BUILD_OPTIONS .
elif [[ -n "$PIPELINE_IMAGE_NAME" ]] ; then
    docker build "$@" --tag "${PIPELINE_IMAGE_NAME}" .
else
    docker build "$@" --tag "${LOCAL_IMAGE_NAME}" .
fi
