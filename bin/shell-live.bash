#!/usr/bin/env bash
BIN_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]:-$0}";)" &>/dev/null && pwd 2>/dev/null;)";

cd "$BIN_DIR/.." || exit
docker run -it --rm \
    --env-file .env \
    --mount type=bind,source="${PWD}",destination=/app \
    --entrypoint bash \
    --workdir /kit \
    kit
